
function goToPage(pageId) {
    document.querySelector(`#${pageId}`).scrollIntoView({behavior: 'smooth'});
}
